#https://github.com/positron96/dockerfiles/blob/master/mcu-eclipse-arm/Dockerfile
FROM ubuntu:18.04

MAINTAINER davidmbroin@gmail.com

# Install dependencies
RUN apt update && apt install -y \
    wget \
    make \
    git \
 && rm -rf /var/lib/apt/lists/*

# Download and extract GNU ARM Toolchain from xPack
RUN mkdir /ttt && cd /ttt && \
 wget -qO- https://api.github.com/repos/xpack-dev-tools/arm-none-eabi-gcc-xpack/releases/latest | sed -n 's/.*browser_download_url.*\(https.*linux.*x64.*tar.gz\)"/\1/p' | wget -qi - \
 && tar -xzf *.tar.gz \
 && rm *.tar.gz \
 && mv $(ls) /opt/arm-none-eabi-gcc \
 && cd / && rm -rf /ttt

# git clone firmware_v3 && clean
RUN git clone https://github.com/epernia/firmware_v3.git \
 && cd firmware_v3 \
 && rm -rf documentation examples scripts templates test
 

# Add & gcc to PATH
ENV PATH="${PATH}:/opt/arm-none-eabi-gcc/bin"